//
//  ViewController.m
//  seney_p05
//
//  Created by Nicholas Ryan Seney on 3/27/17.
//  Copyright © 2017 Nicholas Ryan Seney. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()


@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    [self puzzleInit];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)puzzleInit
{
    NSString *temp = @"";
    
    int negFlag = 1;
    for(UIStackView* view in _superStackView.arrangedSubviews)
    {
        for(tileView* tview in view.arrangedSubviews)
        {
            NSLog(@"%@", temp);
            int color =(int) arc4random()%5;
            if(color < 0)
                color = color*-1;
            NSLog(@"%d", color);
            switch(color)
            {
                case 0:
                    [tview setSol:[UIColor redColor]];
                    temp = [temp stringByAppendingString:(@"r")];
                    _fullWeight+=(1*negFlag);
                    break;
                case 1:
                    [tview setSol:[UIColor orangeColor]];
                    temp =[temp stringByAppendingString:(@"o")];
                    _fullWeight+=(2*negFlag);
                    break;
                case 2:
                    [tview setSol:[UIColor greenColor]];
                    temp =[temp stringByAppendingString:(@"g")];
                    _fullWeight+=(3*negFlag);
                    break;
                case 3:
                    [tview setSol:[UIColor blueColor]];
                   temp = [temp stringByAppendingString:(@"b")];
                    _fullWeight+=(4*negFlag);
                    break;
                case 4:
                    [tview setSol:[UIColor yellowColor]];
                    temp =  [temp stringByAppendingString:(@"y")];
                    _fullWeight+=(5*negFlag);
                    break;
                    
            }
            if(negFlag < 0)
                [tview negateWeight];
            
        }
        negFlag = negFlag*-1;
    }
    temp = [NSString stringWithFormat:@"0\t%d",_fullWeight];
    [_myLabel setText:temp];
}


-(IBAction)changeMyColorRed:(id) sender
{
    _myColor = [UIColor redColor];
    for(UIStackView* view in _superStackView.arrangedSubviews)
    {
        for(tileView* tview in view.arrangedSubviews)
        {
            [tview changeColor:_myColor];
        }
    }
}
-(IBAction)changeMyColorOrange:(id) sender
{
    _myColor = [UIColor orangeColor];
    for(UIStackView* view in _superStackView.arrangedSubviews)
    {
        for(tileView* tview in view.arrangedSubviews)
        {
            [tview changeColor:_myColor];
        }
    }
}

-(IBAction)changeMyColorGreen:(id) sender
{
    _myColor = [UIColor greenColor];
    for(UIStackView* view in _superStackView.arrangedSubviews)
    {
        for(tileView* tview in view.arrangedSubviews)
        {
            [tview changeColor:_myColor];
        }
    }
}

-(IBAction)changeMyColorBlue:(id) sender
{
    _myColor = [UIColor blueColor];
    for(UIStackView* view in _superStackView.arrangedSubviews)
    {
        for(tileView* tview in view.arrangedSubviews)
        {
            [tview changeColor:_myColor];
        }
    }
}

-(IBAction)changeMyColorYellow:(id) sender
{
    _myColor = [UIColor yellowColor];
    for(UIStackView* view in _superStackView.arrangedSubviews)
    {
        for(tileView* tview in view.arrangedSubviews)
        {
            [tview changeColor:_myColor];
        }
    }
}

-(IBAction)enterButton:(id)sender
{
    NSString* temp;
    int newWeight = 0;
    for(UIStackView* view in _superStackView.arrangedSubviews)
    {
        for(tileView* tview in view.arrangedSubviews)
        {
            newWeight+= [tview getDiffWeight];
        }
     
    }
    if(newWeight == 0)
        [_myLabel setText:@"You Win!"];
    else
    {
        temp = [NSString stringWithFormat:@"%d\t%d",newWeight,_fullWeight];
        [_myLabel setText:temp];
    }
}

-(IBAction) resetButton:(id)sender
{
    for(UIStackView* view in _superStackView.arrangedSubviews)
    {
        for(tileView* tview in view.arrangedSubviews)
        {
            [tview resetBut];
        }
        
    }
}

-(IBAction) newGameButton:(id) sender
{
    for(UIStackView* view in _superStackView.arrangedSubviews)
    {
        for(tileView* tview in view.arrangedSubviews)
        {
            [tview resetBut];
        }
        
    }
    [self puzzleInit];
}
@end
