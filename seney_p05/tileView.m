//
//  tileView.m
//  seney_p05
//
//  Created by Nicholas Ryan Seney on 3/27/17.
//  Copyright © 2017 Nicholas Ryan Seney. All rights reserved.
//

#import "tileView.h"

@implementation tileView

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(instancetype) initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        [self commonInit];
    }
    _tileColor = [UIColor whiteColor];
    _negateWeights = false;
    return self;
}

-(instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if(self)
    {
        [self commonInit];
    }
    return self;
}

-(void) commonInit
{
    _customConstraints = [[NSMutableArray alloc] init];
    UIView *view = nil;
    NSArray *objects = [[NSBundle mainBundle] loadNibNamed:@"tileView"
                                                     owner:self
                                                   options:nil];
    for (id object in objects) {
        if ([object isKindOfClass:[UIView class]]) {
            view = object;
            break;
        }
    }
    
    if (view != nil) {
        _containerView = view;
        view.translatesAutoresizingMaskIntoConstraints = NO;
        [self addSubview:view];
        [self setNeedsUpdateConstraints];
    }
}

- (void)updateConstraints
{
    [self removeConstraints:self.customConstraints];
    [self.customConstraints removeAllObjects];
    
    if (self.containerView != nil) {
        UIView *view = self.containerView;
        NSDictionary *views = NSDictionaryOfVariableBindings(view);
        
        [self.customConstraints addObjectsFromArray:
         [NSLayoutConstraint constraintsWithVisualFormat:
          @"H:|[view]|" options:0 metrics:nil views:views]];
        [self.customConstraints addObjectsFromArray:
         [NSLayoutConstraint constraintsWithVisualFormat:
          @"V:|[view]|" options:0 metrics:nil views:views]];
        
        [self addConstraints:self.customConstraints];
    }
    
    [super updateConstraints];
}

-(void)changeColor: (UIColor*) myColor
{
    _tileColor = myColor;
}

-(void) touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event
{
    [_colorview setBackgroundColor:_tileColor];
    if([_solution isEqual:[UIColor redColor]])
        _currentWeight = 1;
    else if([_solution isEqual:[UIColor orangeColor]])
        _currentWeight = 2;
    else if([_solution isEqual:[UIColor greenColor]])
        _currentWeight = 3;
    else if([_solution isEqual:[UIColor blueColor]])
        _currentWeight = 4;
    else if([_solution isEqual:[UIColor yellowColor]])
        _currentWeight = 5;
    
    if(_negateWeights)
        _currentWeight = _currentWeight * -1;
}

-(void)tInit:(UIColor*) myColor
{
    [_colorview setBackgroundColor:myColor];
}
-(void)setSol:(UIColor*) myColor
{
    _solution = myColor;
    if([_solution isEqual:[UIColor redColor]])
        _weight = 1;
    else if([_solution isEqual:[UIColor orangeColor]])
        _weight = 2;
    else if([_solution isEqual:[UIColor greenColor]])
        _weight = 3;
    else if([_solution isEqual:[UIColor blueColor]])
        _weight = 4;
    else if([_solution isEqual:[UIColor yellowColor]])
        _weight = 5;
    else
        _weight = 0;
    
    if(_negateWeights)
        _weight = _weight * -1;
}

-(int)getWeight
{
    return _weight;
}

-(void)negateWeight
{
    _negateWeights = true;
}

-(int)getDiffWeight
{
    int difference;
    if(_negateWeights)
    {
        difference = _weight + _currentWeight;
    }
    else
    {
        difference = _weight - _currentWeight;
    }
    return difference;
}

-(void)resetBut
{
    [_colorview setBackgroundColor:[UIColor whiteColor]];
}

@end
