//
//  tileView.h
//  seney_p05
//
//  Created by Nicholas Ryan Seney on 3/27/17.
//  Copyright © 2017 Nicholas Ryan Seney. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface tileView : UIView

@property (nonatomic, strong) UIView *containerView;
@property (nonatomic, strong) NSMutableArray *customConstraints;
@property (nonatomic, strong) IBOutlet UIView *colorview;
@property(strong, nonatomic) UIColor* tileColor;
@property(strong, nonatomic) UIColor* solution;
@property int weight;
@property int currentWeight;
@property bool negateWeights;

-(void)changeColor: (UIColor*) myColor;
-(void)tInit:(UIColor*) myColor;
-(void)setSol:(UIColor*) myColor;
-(int)getWeight;
-(void) negateWeight;
-(int)getDiffWeight;
-(void)resetBut;
@end
