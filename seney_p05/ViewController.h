//
//  ViewController.h
//  seney_p05
//
//  Created by Nicholas Ryan Seney on 3/27/17.
//  Copyright © 2017 Nicholas Ryan Seney. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "tileView.h"
@interface ViewController : UIViewController

@property (nonatomic,retain) IBOutlet UIStackView *superStackView;

@property (nonatomic,retain) IBOutlet UIButton *redButton;
@property (nonatomic,retain) IBOutlet UIButton *orangeButton;
@property (nonatomic,retain) IBOutlet UIButton *greenButton;
@property (nonatomic,retain) IBOutlet UIButton *blueButton;
@property (nonatomic,retain) IBOutlet UIButton *yellowButton;
@property (nonatomic,retain) IBOutlet UILabel *myLabel;
@property(strong, nonatomic) UIColor* myColor;
@property int fullWeight; 



@end

